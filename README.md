# Open Up Media support information

Adds a link in the admin menu that triggers a modal with OUM support information so clients can 
easily contact us.

## Installation
This module can be installed using Composer through Open Up Media's private 
package repository: <https://packages.openupmedia.be/>.

To do this, add the package repository to the project's `composer.json`
file by adding the following

```
{
  "repositories": [{
    "type": "composer",
    "url": "https://packages.openupmedia.be/"
  }]
}
```

or run

```
composer config repositories.openupmedia composer https://packages.openupmedia.be/
```

You'll then be able to install the module with

```
composer require openupmedia/oum_support
```


## Roadmap

### Add Teamwork Desk or Projects issue form?
Useful starting point: https://www.drupal.org/project/jira_issue_collector
Desk form: https://pm.openupmedia.be/desk/settings/apps/contact-forms/109
Projects exmple form: https://pm.openupmedia.be/#/projects/397132/forms/149
