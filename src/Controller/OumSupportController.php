<?php

/**
 * @file
 * OumSupportController class.
 */

namespace Drupal\oum_support\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;

class OumSupportController extends ControllerBase {
  public function modal() {
    $modal_title = t('Need any help?');

    $modal_body = '
      <div class="oum-support--content">
        <p>' . t('You can contact Open Up Media support via') . '</p>
        <ul>
        <li>Support site: <a href="https://support.openupmedia.be" title="Support website">support.openupmedia.be</a></li>
        <li>E-mail: <a href="mailto:support@openupmedia.be" title="E-mail">support@openupmedia.be</a></li>
        <li>Phone: <a href="tel:+3233372627" title="Phone">+32 3 337 26 27</a></li>
        </ul>
        <p>' . t('You can also visit our <a href="https://www.openupmedia.be" title="Our website">website</a>') . '</p>
      </div>
    ';
    
    $options = [
      'dialogClass' => 'oum-support-popup',
      'width' => '80%',
      'maxWidth' => '800',
    ];
    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand($modal_title, $modal_body, $options));
    
    return $response;
  }
}
